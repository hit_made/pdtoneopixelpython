import led_controller
from pythonosc import osc_server
from pythonosc import dispatcher
from os import uname

c_enable_debug = True

c_ip = "127.0.0.1"
c_port = 5005 #todo

server = None
LedDispacher = None

is_rpi = False

def led_l_dispatch(unused_addr, args, led_r,led_g,led_b):

    if c_enable_debug:
        print("[{0}] ~ {1} {2} {3}".format(args[0], led_r,led_g,led_b))
    if is_rpi:
        led_controller.fill_partial(0, int(led_controller.LED_COUNT / 2) , (led_r, led_g, led_b))



def led_r_dispatch(unused_addr, args, led_r,led_g,led_b):
  if c_enable_debug:
    print("[{0}] ~ {1} {2} {3}".format(args[0], led_r,led_g,led_b))
    if is_rpi:
        led_controller.fill_partial(int(led_controller.LED_COUNT / 2), led_controller.LED_COUNT, (led_r, led_g, led_b))


def init():
    global is_rpi
    init_connection()
    if uname().nodename == 'raspberrypi':
        is_rpi = 1
        print("hi pi.")

    if is_rpi:
        led_controller.init_leds()
        led_controller.clear_leds()

def init_connection():
    global server
    global LedDispacher

    LedDispacher = dispatcher.Dispatcher()
    LedDispacher.map("/Debug", print)
    LedDispacher.map("/leds_l",led_l_dispatch,"Leds Left")
    LedDispacher.map("/leds_r", led_r_dispatch, "Leds Right")
    server = osc_server.ThreadingOSCUDPServer((c_ip,c_port), LedDispacher)


def main():
    global server
    print("Serving on {}".format(server.server_address))
    server.serve_forever()


if __name__ == "__main__":
    init()
    main()
