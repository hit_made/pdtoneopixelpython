from neopixel import *



strip = None


# LED strip configuration:
LED_COUNT      = 16      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 128     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53


def init_leds():
    global strip
    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)#fixme
    # Intialize the library (must be called once before other functions).
    strip.begin()


def clear_leds():
     global strip

     for i in range(strip.numPixels()):
        strip.setPixelColor(i, Color(0,0,0))

    #strip.show()


def fill_leds(color):
    global strip


    for i in range(strip.numPixels()):
        strip.setPixelColor(i, Color(color[1],color[0],color[2]))

     #strip.show()


def fill_partial(start,end,color):
    global strip
    for i in range(start,end):
        strip.setPixelColor(i, Color(color[1], color[0], color[2]))
    strip.show()

def show_leds_from_list(led_idx_list,color):
    global strip

    #step 1: clear all leds.
    clear_leds()


    for i in led_idx_list:
        strip.setPixelColor(i,Color(color[1],color[0],color[2]))



def update_leds():
    global strip
    strip.show()